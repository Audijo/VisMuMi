class Nothing {

  Nothing() {
     
  }
  
  void display() {
    background(0); 
    stroke(255);
    fill(255);
    PFont f= createFont("Helvetica Neue fein", 64);
    String s1= "Das habe ich nicht verstanden";
    String s2= "Versuch es noch einmal";
    textFont(f);
    textSize(20);
    textAlign(CENTER);
    text(s1, width/2, 250);
    text(s2, width/2, 280);
    
  }
}
