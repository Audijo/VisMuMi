class Char
{
  int freqChar;
  int numChar;
  float xpos, ypos;
  float nx,ny;

  Char(int a, int count)
  {
    freqChar=a;
    numChar = count;
  }

  void display(int index)
  {
    noStroke();
    fill(140, 170, 255, 200);
    
    float angleIncrement = TWO_PI / numChar;
    //     offset    scaler       pick the angle based on whichone/total
    xpos = width/2 +  300 * cos( index*angleIncrement); 
    ypos = height/2 + 500 * sin( index*angleIncrement);
    xpos += nx;
    ypos += ny;
    ellipse(xpos, ypos, freqChar*7, freqChar*7);
  }
}

