class Idle {
  
  PImage headset;
  PImage pfeil;

  Idle() {
    headset = loadImage("files/headset.png");
  }
  
  void display() {
    background(0);
    stroke(255);
    fill(255);
    image(headset, (width/2)-250, 580);
    PFont f= createFont("Helvetica Neue fein", 100);
    String s1= "VeeMee";
    String s2= "Audiovisuelle Textinterpretation";
    String s3= "1.) Setze das Headset auf und drücke START";
    String s4= "2.) VeeMee schlägt dir ein Thema vor, zu dem";
    String s5= "du acht Sekunden etwas sagen kannst";
    String s6= "3.) VeeMee zeigt dir dann seine audiovisuelle Übersetzung";
    
    textFont(f);
    textSize(80);
    textAlign(CENTER);
    text(s1, (width/2), 280);
    
    textFont(f);
    textSize(18);
    textAlign(CENTER);
    text(s2, (width/2)+6, 310);
    
    textFont(f);
    textSize(20);
    textAlign(CENTER);
    text(s3, (width/2)-35, 400);
    
    textFont(f);
    textSize(20);
    textAlign(CENTER);
    text(s4, (width/2)-26, 430);
    
    textFont(f);
    textSize(20);
    textAlign(CENTER);
    text(s5, (width/2)-33, 460);

    textFont(f);
    textSize(20);
    textAlign(CENTER);
    text(s6, (width/2)+34, 490);
  }
}
