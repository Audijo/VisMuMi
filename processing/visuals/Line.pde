class Line{
  float   xStart;
  float   yStart;
  float   xEnd;
  float   yEnd;
  float   xCurrent;
  float   yCurrent;
  color   lineColor;
  int     startTime;
  float   duration;
  int     counter;
  
  Line(float xS, float yS, float xE, float yE, float dur, color col){
    xStart   = xS;
    yStart   = yS;
    xEnd     = xE;
    yEnd     = yE;
    xCurrent = (xS);
    yCurrent = (yS);
    startTime= millis();
    lineColor= col;
    duration = dur*1000;
  }

  void display(int index){
    stroke(lineColor);
    strokeWeight(2);
    if ((counter-startTime) < duration) {
      counter = millis();
    }
    line(xStart, yStart, map((counter-startTime), 0, duration, xStart, xEnd), map((counter-startTime), 0, duration, yStart, yEnd));
  }
}
