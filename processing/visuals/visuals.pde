import oscP5.*;
import netP5.*;
  
OscP5 oscP5;
int currentState = 3;//999;
Idle idle;
Recording recording;
Recognizing recognizing;
Translating translating;
Nothing nothing;

int numChar = 29;

boolean egg = false;


void setup() {
  size(768,1280);
  frameRate(20);
  /* start oscP5, listening for incoming messages at port 12000 */
  oscP5 = new OscP5(this,12000);
  idle = new Idle();
  recognizing = new Recognizing();
  translating = null;
  nothing = new Nothing();
}


void draw() {
  switch(currentState) {
    case 0: 
      translating = null;
      egg = false;
      idle.display();
      break;
    case 1: 
      recording.display();
      break;
    case 2: 
      recognizing.display();
      break;
    case 3: 
      //println("Translate");
      if (translating != null) {
        translating.display();
      }
      break;
    case 4: 
      //println("Nothing Recognized");
      nothing.display();
      break;
  }  
}

/* incoming osc message are forwarded to the oscEvent method. */
void oscEvent(OscMessage msg) {
  /* print the address pattern and the typetag of the received OscMessage */
  print("### received an osc message.");
  print(" addrpattern: " + msg.addrPattern());
  println(" typetag: " + msg.typetag());
  
  if(msg.checkAddrPattern("/p/state")) {
    if(msg.checkTypetag("i")) {
      if (msg.get(0).intValue() == 1) {
        recording = new Recording();
      }
      currentState = msg.get(0).intValue();
      return;
    } else {
      println("Wrong type tag for state message");
      return;
    } 
  }
  
  if(msg.checkAddrPattern("/p/rec")) {
    if(msg.checkTypetag("ii")) {
      if (msg.get(0).intValue() == 1) {
        recording.startBar(msg.get(1).intValue());
      } //else {
        //recording.stopBar();
      //}
      return;
    } else {
      println("Wrong type tag for rec message");
      return;
    } 
  }
  
  if(msg.checkAddrPattern("/p/trans")) {
    float totalDuration = 0;
    int[] freqChar = new int[numChar];
    println((msg.typetag().length()-numChar-1));
    int[] charSequence = new int[(msg.typetag().length()-numChar-1)];
    println("Total Duration");
    totalDuration = msg.get(0).floatValue();
    println("Char Frequency");
    for (int i=0; i<numChar; i++) {
      freqChar[i] = msg.get(i+1).intValue();
    }
    println("Char Sequence");
    for (int l=0; l<(msg.typetag().length()-numChar-1); l++) {
      charSequence[l] = msg.get(1+numChar+l).intValue();
    }
    println("Instanciate Translating");
    translating = new Translating(egg, numChar, totalDuration, freqChar, charSequence);
  }

  if(msg.checkAddrPattern("/p/egg")) {
    egg = true;
  }
  
  
}

