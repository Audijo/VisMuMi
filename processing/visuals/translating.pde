class Translating {
  
  int numChar;
  int[] freqChar;
  Char[] chars;
  ArrayList<Line> lines = new ArrayList<Line>();
  float totalDuration;
  int[] charSequence;
  
  int wordCount = 0;
  ArrayList<IntList> words = new ArrayList<IntList>();
  int longestWord = 0;

  PFont f= createFont("Helvetica Neue fein", 64);
  String s1= "Schneewittchen";
  
  Translating(boolean egg, int in_numChars, float in_totalDuration, int[] in_freqChar, int[] in_charSequence) {
    numChar       = in_numChars;
    totalDuration = in_totalDuration;
    freqChar      = in_freqChar;
    charSequence  = in_charSequence;
    
    chars = new Char[numChar];
    
    initializeChars();

    for (int i=0; i<numChar; i++) {
      chars[i].display(i);
    }
    
    for (int i=0; i<int(charSequence.length); i=i+2) {
      println(charSequence[i]);
      if (charSequence[i] == 99) {
        words.add(new IntList());
        wordCount += 1;
        println(words);
        println("Seperator 99");
      } else {
        words.get(wordCount-1).append(charSequence[i]);
        words.get(wordCount-1).append(charSequence[i+1]);
      }
    }

    for (int i=0; i<words.size(); i++) {
      color col = color(random(100, 255), random(100, 255), random(100, 255), 255);
      for (int j=0; j<words.get(i).size(); j=j+2) {
        lines.add(new Line(chars[words.get(i).get(j)].xpos, 
                            chars[words.get(i).get(j)].ypos,
                            chars[words.get(i).get(j+1)].xpos,
                            chars[words.get(i).get(j+1)].ypos,
                            totalDuration, col));
      }
    }

  }

  void display() {
    background(0);
    // text(frameRate,width/2,50);
    
    for (int i=0; i<lines.size(); i++) {
      lines.get(i).display(i);
    }
  
    for (int i=0; i<numChar; i++) {
      chars[i].display(i);
    }

    if (egg == true) {
      textFont(f);
      textSize(20);
      textAlign(CENTER);
      text(s1, width/2, (height/2)+550);
    }

  }

  void initializeChars() {
    for (int i=0; i<numChar; i++)
    {
      //Construct a new person (10 times). Pass in each row of data from the lists. 
      chars[i] = new Char(freqChar[i], numChar);
    }
  }
}
