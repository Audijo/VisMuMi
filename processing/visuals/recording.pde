class Recording {
  String [] words = {"Liebe", "Farbe", "Pusteblume",
                   "Licht", "Gegenteil", "Sternenhimmel",
                   "Tanz", "Bewegung", "Fortschritt",
                   "Zufriedenheit", "Selbstzweifel", "Wegweiser",
                   "Tasten", "Jazz", "Blasmusik",
                   "Nintendo", "Pokemon", "SuperMario",
                   "Labyrinth", "Verkehr", "Zielgerade",
                   "Kälte", "Geschmack", "Streusel",
                   "Sand", "Hitze", "Oase",
                   "Rückrunde", "Zufriedenheit", "Zuversicht",
                   "Sündenbock", "Wetter", "Kontrolle",
                   "Vertrauen",};
  
  Bar bar;
  int index;

  Recording() {
    index = int(random(0, words.length));
  }

  
  void display() {
    background(0); 
    stroke(255);
    fill(255);
    PFont f= createFont("Helvetica Neue fein", 64);
    String s= "Ich höre dir zu";
    textFont(f);
    textSize(30);
    textAlign(CENTER);
    text(words[index], width/2, 250);
    
    if (bar != null) {
      text(s, width/2, 780);
      bar.display();
    }
    
  }
  
  void startBar(int time) {
    bar = new Bar(time);
  }
  
  void stopBar() {
    bar = null;
  }
}

class Bar {
  int startTime;
  int counter;
  int maxTime; 
  
  Bar(int in_maxTime) {
    counter = 0;
    startTime= millis();
    maxTime = in_maxTime;
  }
  
  void display() {
    if (counter-startTime < maxTime) {
      counter=millis();
    }
    
    fill(255);
    noStroke();
    rect(40,700,map(counter-startTime,0,maxTime,0,width-80), 19 );
    
    noFill();
    stroke(255);
    rect(40,700,width-80,19);
  }
}
