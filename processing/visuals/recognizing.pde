class Recognizing {
  
  int numberOfCircle = 12;
  int splitSin = 5000;
  float sinWave[];
  float speed = 1;
  
  int counter= 0;
  int distance = 150;
  int b;
  
  boolean lightSignal[] = {false,false,false,false,false,
                           false,false,false,false,false};
  boolean nextSignal = false;

  Recognizing() {
    sinWave = new float[ splitSin ];
    for(int i = 0 ; i < splitSin ; i++) {
      sinWave[i] = sin(TWO_PI / splitSin * i);
    }
  }
  
  void display() {
    background(0);
    stroke(255);
    DrawLights();
  }
  
  void DrawLights(){
    for(int i = 0 ; i < numberOfCircle ; i++) {
      pushMatrix();//
      translate( sin( (TWO_PI/numberOfCircle) * i ) * distance , cos( (TWO_PI/numberOfCircle) * i ) * distance );
      
      float waveData = sin( (TWO_PI/splitSin)* millis() - TWO_PI/12 * i );
      float pow2sin = pow(waveData + abs(waveData) , 2 );
      b = (int)(127* (1+ map(pow2sin , 0 , 4 , -1 , 1) ) );
      
      
      fill(b);
      noStroke();
      ellipse(width/2 , height/2 , 50 , 50);
      
      popMatrix(); 
    } 
    counter++;
  }
}
