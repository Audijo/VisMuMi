#!/usr/bin/env python

import signal
import sys
import logging

import time
import struct
from array import array
import pyaudio
import audioop
import speech_recognition as sr
import wave


class BrianRec():
    def __init__(self, com, logger):
        self.p = pyaudio.PyAudio()
        # Init the recognizer
        self.recog = sr.Recognizer("de-DE")

        self.com = com
        self.logger = logger

        # for i in range(self.p.get_device_count()):
        #   print (self.p.get_device_info_by_index(i))

        # Open input stream
        self.stream = self.p.open(format=pyaudio.paInt16,
                             channels=1,
                             rate=44100,
                             input=True,
                             frames_per_buffer=4096,
                             input_device_index=4)

    def deinit(self):
        self.stream.close()

    def record(self):

        recording = False
        t_init = time.time() + 1
        rec_buffer = array('h')
        silence_count = 0

        while True:
            data = array('h', self.stream.read(4096))
            rms = audioop.rms(data, 2)

            # First RMS values are way off, so wait a sec
            if time.time() < t_init:
                continue

            # Start the recording
            if rms > 500 and not recording:
                self.logger.debug("Start Recording")
                self.com.pSendRecording(1, 8000)
                recording = True
                t_max = time.time() + 8

            if rms < 500 and recording:
                silence_count = silence_count + 1
            else:
                silence_count = 0

            # Record
            if recording:
                rec_buffer.extend(data)

            # Stop recording after some time
            if recording and ((time.time() > t_max) or (silence_count == 20)):
                self.logger.debug("Stop Recording")
                self.com.pSendRecording(0, 0)
                recording = False
                # print(silence_count)
                break

        # Pack the recorded audio as wave
        data = struct.pack('<' + ('h'*len(rec_buffer)), *rec_buffer)
        sample_width = self.p.get_sample_size(pyaudio.paInt16)
        wf = wave.open("/home/pi/tmp/mic.wav", 'wb')
        wf.setnchannels(1)
        wf.setsampwidth(sample_width)
        wf.setframerate(44100)
        wf.writeframes(data)
        wf.close()

    def recognize(self):
        # Open the just saved wave
        with sr.WavFile("/home/pi/tmp/mic.wav") as source:
            # Recognize the audio
            audio = self.recog.record(source)

        # Print the result if any
        try:
            result = self.recog.recognize(audio)
            self.logger.debug("You said: " + result)

        except LookupError:
            # speech is unintelligible
            result = None
            self.logger.debug("Could not understand audio")

        return result


def signal_handler(signal, frame):
    print('You pressed Ctrl+C!')
    sys.exit(0)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    brianRec = BrianRec()
    brianRec.record_and_analyze()
