#!/usr/bin/env python
# -*- coding: utf-8 -*-

import liblo
import sys


class BrianOSC():
    def __init__(self):
        self.target_sc = None
        self.target_p = None

        # send all messages to port 57120 on the local machine
        try:
            self.target_sc = liblo.Address(57120)
        except liblo.AddressError, err:
            print str(err)
            sys.exit()

        try:
            self.target_p = liblo.Address(12000)
        except liblo.AddressError, err:
            print str(err)
            sys.exit()

    def playNes2(self, char):
        msg = liblo.Message("/sc/play")
        if char == u"ä":
            msg.add("ae")
        elif char == u"ü":
            msg.add("ue")
        elif char == u"ö":
            msg.add("oe")
        else:
            msg.add(char)

        liblo.send(self.target_sc, msg)

    def pSendState(self, state):
        msg = liblo.Message("/p/state")
        msg.add(state)

        liblo.send(self.target_p, msg)

    def pSendRecording(self, status, maxTime):
        msg = liblo.Message("/p/rec")
        msg.add(int(status))
        msg.add(int(maxTime))

        liblo.send(self.target_p, msg)

    def startP(self, totalDuration, charCounts, messageContent):
        msg = liblo.Message("/p/trans")
        msg.add(float(totalDuration))
        for count in charCounts:
            msg.add(int(count))
        for entry in messageContent:
            msg.add(int(entry))

        liblo.send(self.target_p, msg)

    def egg(self):
        msg = liblo.Message("/p/egg")

        liblo.send(self.target_p, msg)
# send message "/foo/message1" with int, float and string arguments
# liblo.send(target, "/sc/play", 1)

# time.sleep(10)

# liblo.send(target, "/sc/play", 0)

# liblo.send(target, "/sc/eventStream", "atari2600", 6, 0.25, 0.25, 0.25, 0.45, 0.65, 0.45, 4)
# liblo.send(target, "/sc/eventStream", "\\atari2600", ('i', 4), ('f', 0.25), ('f', 0.25), ('f', 0.25), ('f', 0.45), ('i', 2) )


# # send double, int64 and char
# liblo.send(target, "/foo/message2", ('d', 3.1415), ('h', 2**42), ('c', 'x'))

# # we can also build a message object first...
# msg = liblo.Message("/foo/blah")
# # ... append arguments later...
# msg.add(123, "foo")
# # ... and then send it
# liblo.send(target, msg)

# # send a list of bytes as a blob
# blob = [4, 8, 15, 16, 23, 42]
# liblo.send(target, "/foo/blob", blob)

# # wrap a message in a bundle, to be dispatched after 2 seconds
# bundle = liblo.Bundle(liblo.time() + 2.0, liblo.Message("/blubb", 123))
# liblo.send(target, bundle)

if __name__ == "__main__":
    app = BrianOSC()
    app.run()
