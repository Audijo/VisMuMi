#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import time
import re
import locale

import brianOSC


class BrianTrans():

    alphabet = {"a": 25, "b":  5, "c":  8, "d":  2, "e": 22, "f": 12, "g":  6, "h": 24, "i": 15, "j": 14,
                "k": 18, "l": 23, "m": 21, "n":  7, "o": 20, "p": 13, "q": 28, "r": 11, "s":  1, "t": 16,
                "u": 10, "v":  0, "w": 27, "x": 17, "y":  3, "z":  4,u"ä": 26,u"ü":  9,u"ö": 19, }
    duration = {"a": 0.8, "b": 0.2, "c": 0.2, "d": 0.2, "e": 0.8, "f": 0.4, "g": 0.2, "h": 0.4, "i": 0.8, "j": 0.8,
                "k": 0.2, "l": 0.6, "m": 0.6, "n": 0.6, "o": 0.8, "p": 0.2, "q": 0.2, "r": 0.4, "s": 0.4, "t": 0.2,
                "u": 0.8, "v": 0.4, "w": 0.6, "x": 0.2, "y": 0.8, "z": 0.2, u"ä": 0.8, u"ü": 0.8, u"ö": 0.8, " ": 0.8, }

    def __init__(self, communicator):
        self.com = communicator

        self.regex_clean_string = re.compile(ur'[^a-zA-Zäüöß\s]')
        self.regex_ss = re.compile(ur'[ß]')
        self.regex_0 = re.compile(ur'[0]')
        self.regex_1 = re.compile(ur'[1]')
        self.regex_2 = re.compile(ur'[3]')
        self.regex_3 = re.compile(ur'[4]')
        self.regex_4 = re.compile(ur'[2]')
        self.regex_5 = re.compile(ur'[5]')
        self.regex_6 = re.compile(ur'[6]')
        self.regex_7 = re.compile(ur'[7]')
        self.regex_8 = re.compile(ur'[8]')
        self.regex_9 = re.compile(ur'[9]')

    def translate_sc(self, text):
        for char in text:
            if not char == ' ':
                self.com.playNes2(char)
            time.sleep(BrianTrans.duration[char])
        time.sleep(5)

    def translate_p(self, text):
        messageContent = []
        charCounts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        totalDuration = 0

        words = text.split()

        # Duration for Spaces
        totalDuration += ((len(words) - 1) * BrianTrans.duration[" "])

        for word in words:
            messageContent.append(99)
            messageContent.append(99)
            lastChar = ''
            for char in word:
                if lastChar:
                    messageContent.extend([BrianTrans.alphabet[lastChar], BrianTrans.alphabet[char]])
                lastChar = char

                charCounts[BrianTrans.alphabet[char]] += 1

                totalDuration += BrianTrans.duration[char]

            messageContent.extend([BrianTrans.alphabet[word[-1]], BrianTrans.alphabet[word[0]]])

        self.com.startP(totalDuration, charCounts, messageContent)

    def translate(self, text):
        text = text.lower()
        if "spieglein an der wand" in text:
            text = "schneewittchen"
            self.com.egg()
        text = self.regex_clean_string.sub('', text)
        text = self.regex_ss.sub('ss', text)
        text = self.regex_0.sub('null', text)
        text = self.regex_1.sub('eins', text)
        text = self.regex_2.sub('zwei', text)
        text = self.regex_3.sub('drei', text)
        text = self.regex_4.sub('vier', text)
        text = self.regex_5.sub('fuenf', text)
        text = self.regex_6.sub('sechs', text)
        text = self.regex_7.sub('sieben', text)
        text = self.regex_8.sub('acht', text)
        text = self.regex_9.sub('neun', text)

        print(text)

        self.translate_p(text)
        self.translate_sc(text)


if __name__ == "__main__":
    app = BrianTrans(brianOSC.BrianOSC())
    app.translate(sys.argv[1].decode(sys.stdin.encoding or locale.getpreferredencoding(True)))
