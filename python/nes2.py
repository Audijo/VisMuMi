

class Nes2setting():
    register_map = {
        # [ byte, start_bit, bit_length ]
        'sqwav1_dutyCycle':             [0,  6, 2],  # a0
        'sqwav1_loopEnvelope':          [0,  5, 1],
        'sqwav1_envelopeDecayDisable':  [0,  4, 1],
        'sqwav1_envelopeDecayRate':     [0,  0, 4],

        'sqwav1_sweepOn':               [1,  7, 1],  # a1
        'sqwav1_sweepLength':           [1,  4, 3],
        'sqwav1_sweepIncDec':           [1,  3, 1],
        'sqwav1_sweepShift':            [1,  0, 3],

        'sqwav1_freqLow':               [2,  0, 8],  # a2

        'sqwav1_vblLengthCounter':      [3,  3, 5],  # a3
        'sqwav1_freqHigh':              [3,  0, 3],

        'sqwav2_dutyCycle':             [4,  6, 2],  # b0
        'sqwav2_loopEnvelope':          [4,  5, 1],
        'sqwav2_envelopeDecayDisable':  [4,  4, 1],
        'sqwav2_envelopeDecayRate':     [4,  0, 4],

        'sqwav2_sweepOn':               [5,  7, 1],  # b1
        'sqwav2_sweepLength':           [5,  4, 3],
        'sqwav2_sweepIncDec':           [5,  3, 1],
        'sqwav2_sweepShift':            [5,  0, 3],

        'sqwav2_freqLow':               [6,  0, 8],  # b2

        'sqwav2_vblLengthCounter':      [7,  3, 5],  # b3
        'sqwav2_freqHigh':              [7,  0, 3],

        'triwav_linearCounterStart':    [8,  7, 1],  # c0
        'triwav_linearCounter':         [8,  0, 7],

        'triwav_freqLow':               [9,  0, 8],  # c2

        'triwav_lengthCounter':         [10, 3, 5],  # c3
        'triwav_fregHigh':              [10, 0, 3],

        'noise_loopEnvelope':           [11, 5, 1],  # d0
        'noise_envelopeDecayDisable':   [11, 4, 1],
        'noise_envelopeDecayRate':      [11, 0, 4],

        'noise_shortMode':              [12, 7, 1],  # d2
        'noise_playbackSampleRate':     [12, 0, 4],

        'noise_lengthCounter':          [13, 3, 5],  # d3

        'dmc_irqGenerator':             [14, 7, 1],  # e0
        'dmc_looping':                  [14, 6, 1],
        'dmc_freqControl':              [14, 0, 4],

        'dmc_deltaCountOutDC':          [15, 0, 7],  # e1

        'dmc_addressLoadReg':           [16, 0, 8],  # e2

        'dmc_lengthReg':                [17, 0, 8],  # e3

        'smask_dmcEnabled':             [18, 4, 1],  # smask
        'smack_noiseEnabled':           [18, 3, 1],
        'smack_triWavEnabled':          [18, 2, 1],
        'smack_sqWavCh2Enabled':        [18, 1, 1],
        'smack_sqWavCh1Enabled':        [18, 0, 1]

    }

    def __init__(self, dur=1, amp=0.1,
                 smack_sqWavCh1Enabled=False, smack_sqWavCh2Enabled=False, smack_triWavEnabled=False, smack_noiseEnabled=False,
                 smask_dmcEnabled=False,
                 sqwav1_dutyCycle=0, sqwav1_loopEnvelope=False, sqwav1_envelopeDecayDisable=False,
                 sqwav1_envelopeDecayRate=0, sqwav1_sweepOn=False, sqwav1_sweepLength=0, sqwav1_sweepIncDec=False,
                 sqwav1_sweepShift=0, sqwav1_freqLow=0, sqwav1_freqHigh=0, sqwav1_vblLengthCounter=0,
                 sqwav2_dutyCycle=0, sqwav2_loopEnvelope=False, sqwav2_envelopeDecayDisable=False,
                 sqwav2_envelopeDecayRate=0, sqwav2_sweepOn=False, sqwav2_sweepLength=0, sqwav2_sweepIncDec=False,
                 sqwav2_sweepShift=0, sqwav2_freqLow=0, sqwav2_freqHigh=0, sqwav2_vblLengthCounter=0,
                 triwav_linearCounterStart=False, triwav_linearCounter=0, triwav_freqLow=0, triwav_fregHigh=0,
                 triwav_lengthCounter=0,
                 noise_loopEnvelope=False, noise_envelopeDecayDisable=False, noise_envelopeDecayRate=0, noise_shortMode=False,
                 noise_playbackSampleRate=0, noise_lengthCounter=0,
                 dmc_irqGenerator=False, dmc_looping=False, dmc_freqControl=0, dmc_deltaCountOutDC=0,
                 dmc_addressLoadReg=0, dmc_lengthReg=0
                 ):
        self.dur = dur
        self.amp = amp

        self.smack_sqWavCh1Enabled = smack_sqWavCh1Enabled
        self.smack_sqWavCh2Enabled = smack_sqWavCh2Enabled
        self.smack_triWavEnabled = smack_triWavEnabled
        self.smack_noiseEnabled = smack_noiseEnabled
        self.smask_dmcEnabled = smask_dmcEnabled

        # Square Wave Channel 1
        self.sqwav1_dutyCycle = sqwav1_dutyCycle
        self.sqwav1_loopEnvelope = sqwav1_loopEnvelope
        self.sqwav1_envelopeDecayDisable = sqwav1_envelopeDecayDisable
        self.sqwav1_envelopeDecayRate = sqwav1_envelopeDecayRate
        self.sqwav1_sweepOn = sqwav1_sweepOn
        self.sqwav1_sweepLength = sqwav1_sweepLength
        self.sqwav1_sweepIncDec = sqwav1_sweepIncDec
        self.sqwav1_sweepShift = sqwav1_sweepShift
        self.sqwav1_freqLow = sqwav1_freqLow
        self.sqwav1_freqHigh = sqwav1_freqHigh
        self.sqwav1_vblLengthCounter = sqwav1_vblLengthCounter

        # Square Wave Channel 2
        self.sqwav2_dutyCycle = sqwav2_dutyCycle
        self.sqwav2_loopEnvelope = sqwav2_loopEnvelope
        self.sqwav2_envelopeDecayDisable = sqwav2_envelopeDecayDisable
        self.sqwav2_envelopeDecayRate = sqwav2_envelopeDecayRate
        self.sqwav2_sweepOn = sqwav2_sweepOn
        self.sqwav2_sweepLength = sqwav2_sweepLength
        self.sqwav2_sweepIncDec = sqwav2_sweepIncDec
        self.sqwav2_sweepShift = sqwav2_sweepShift
        self.sqwav2_freqLow = sqwav2_freqLow
        self.sqwav2_freqHigh = sqwav2_freqHigh
        self.sqwav2_vblLengthCounter = sqwav2_vblLengthCounter

        # Triangular Wave
        self.triwav_linearCounterStart = triwav_linearCounterStart
        self.triwav_linearCounter = triwav_linearCounter
        self.triwav_freqLow = triwav_freqLow
        self.triwav_fregHigh = triwav_fregHigh
        self.triwav_lengthCounter = triwav_lengthCounter

        # Noise
        self.noise_loopEnvelope = noise_loopEnvelope
        self.noise_envelopeDecayDisable = noise_envelopeDecayDisable
        self.noise_envelopeDecayRate = noise_envelopeDecayRate
        self.noise_shortMode = noise_shortMode
        self.noise_playbackSampleRate = noise_playbackSampleRate
        self.noise_lengthCounter = noise_lengthCounter

        # Delta Modulation Channel
        self.dmc_irqGenerator = dmc_irqGenerator        # Not in use
        self.dmc_looping = dmc_looping
        self.dmc_freqControl = dmc_freqControl
        self.dmc_deltaCountOutDC = dmc_deltaCountOutDC  # Not in use
        self.dmc_addressLoadReg = dmc_addressLoadReg    # Not in use
        self.dmc_lengthReg = dmc_lengthReg              # Not in use

    def registers(self):
        registers = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        for p_name in Nes2setting.register_map.keys():
            if (int(self.__dict__[p_name]) >= 0) and (int(self.__dict__[p_name]) <= (2 ** Nes2setting.register_map[p_name][2]) - 1):
                # if len(registers) > Nes2setting.register_map[p_name][0]:
                    registers[Nes2setting.register_map[p_name][0]] = (registers[Nes2setting.register_map[p_name][0]] |
                                                                      int(self.__dict__[p_name]) << Nes2setting.register_map[p_name][0])
                # else:
                    # registers.insert(Nes2setting.register_map[p_name][0], int(self.__dict__[p_name]) << Nes2setting.register_map[p_name][0])
            else:
                # TODO: Should we really stop here or better catch the error
                raise ValueError('Parameter out of bounds')

        registers.insert(0, self.amp)
        registers.insert(0, self.dur)

        return registers
