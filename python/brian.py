#!/usr/bin/env python

import sys
import signal
import time
import logging
import logging.handlers

import pigpio

import brianRec
import brianOSC
import brianTrans


class Brian():

    def __init__(self):
        signal.signal(signal.SIGINT, self.signal_handler)

        # Set logging config
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(1)
        # Formatter
        self.formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        # Handler for logfile, 50MB per file, store last 5000 files
        self.RFHhdlr = logging.handlers.RotatingFileHandler("brian.log", maxBytes=52428800, backupCount=5000)
        self.RFHhdlr.setFormatter(self.formatter)
        self.RFHhdlr.setLevel(logging.DEBUG)
        self.logger.addHandler(self.RFHhdlr)
        # Handler for shell output
        self.SHhdlr = logging.StreamHandler()
        self.SHhdlr.setFormatter(self.formatter)
        # Sets log level to WARN going more verbose for each new -v.
        self.SHhdlr.setLevel(logging.DEBUG)
        self.logger.addHandler(self.SHhdlr)

        self.brianOSC = brianOSC.BrianOSC()
        self.brianRec = brianRec.BrianRec(self.brianOSC, self.logger)
        self.brianTrans = brianTrans.BrianTrans(self.brianOSC)

        # Time to add onto accumulated note time
        self.offset_time = 0

        self.lastState = 1
        self.currentState = 0
        self.nextState = 0

        self.recognized_text = ''

        self.pi = pigpio.pi()

        while True:
            self.stateMachine(self.currentState, self.nextState)

    def stateMachine(self, cS, nS):
        if not cS == nS:
            cS = nS

        if cS == 0:
            self.idle()
            return
        elif cS == 1:
            self.record()
            return
        elif cS == 2:
            self.recognize()
            return
        elif cS == 3:
            self.translate()
            return
        elif cS == 4:
            self.nothing_recognized()
            return
        else:
            self.logger.error("Unknown State")
            return

    def idle(self):
        self.logger.debug("> Idle")

        # Send OSC "We are idle"
        self.brianOSC.pSendState(0)

        # Wait for Enter
        #raw_input("Press Enter to proceed")
        if self.pi.wait_for_edge(24):
            self.nextState = 1
            self.start = False
        else:
            self.nextState = 0


        self.lastState = 0
        self.logger.debug("< Idle")
        return

    def record(self):
        self.logger.debug("> Record")

        # Send OSC "We start recording"
        self.brianOSC.pSendState(1)

        # TODO: Timeout and back to idle
        self.brianRec.record()

        self.lastState = 1
        self.nextState = 2
        self.logger.debug("< Record")
        return

    def recognize(self):
        self.logger.debug("> Recognize")

        # Send OSC "We are recognizing"
        self.brianOSC.pSendState(2)

        self.recognized_text = self.brianRec.recognize()

        self.logger.info("TEXT: " + self.recognized_text)

        self.lastState = 2
        self.nextState = 3
        self.logger.debug("< Recognize")
        return

    def translate(self):
        self.logger.debug("> Translate")

        # Send OSC "We are translating"
        self.brianOSC.pSendState(3)

        if self.recognized_text:
            # accumulated_note_time =
            self.brianTrans.translate(self.recognized_text)
            # NOTE: Not needed since we do the waiting in brianTrans
            # time.sleep(accumulated_note_time + self.offset_time)
            self.recognized_text = ''
        else:
            # Nothing recognized
            self.recognized_text = ''
            self.nextState = 4
            self.logger.debug("< Translate")
            return

        self.lastState = 3
        self.nextState = 0
        self.logger.debug("< Translate")
        return

    def nothing_recognized(self):
        self.logger.debug("> Nothing recognized")

        # Send OSC "Nothing recognized"
        self.brianOSC.pSendState(4)

        time.sleep(5)

        self.lastState = 4
        self.nextState = 0
        self.logger.debug("< Nothing recognized")
        return


    def signal_handler(self, signal, frame):
        self.logger.info('You pressed Ctrl+C!')
        self.brianRec.deinit()
        sys.exit(0)

if __name__ == "__main__":

    app = Brian()
