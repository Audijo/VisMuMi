#!/usr/bin/env python

import signal
import sys

import time
import struct
from array import array
import pyaudio
import audioop
import speech_recognition as sr
import wave

import OSC

def signal_handler(signal, frame):
	print('You pressed Ctrl+C!')
	sys.exit(0)

def main():
	signal.signal(signal.SIGINT, signal_handler)

	p = pyaudio.PyAudio()
	# Init the recognizer
	recog = sr.Recognizer("de-DE")

	client = OSC.OSCClient()
	client.connect( ("localhost", 7110) )

	#for i in range(p.get_device_count()):
	#	print (p.get_device_info_by_index(i))

	while True:
		# Open input stream
		stream = p.open(format = pyaudio.paInt16,
			channels = 1,
			rate = 44100,
			input = True,
			frames_per_buffer = 256,
			input_device_index = 2)


		recording = False
		t_init = time.time() + 1
		rec_buffer = array('h')
		silence_count = 0

		while True:
			data = array('h',stream.read(256))
			rms = audioop.rms(data,2)

			# First RMS values are way off, so wait a sec
			if time.time() < t_init:
				continue

			# Start the recording
			if rms > 500 and not recording:
				print("Start Recording")
				recording = True
				t_max = time.time() + 8

				#msg = OSC.OSCMessage()
				#msg.setAddress("/recog/status")
				#msg.append( 1 )
				#client.send(msg)

			if rms < 500 and recording:
				silence_count = silence_count + 1
			else:
				silence_count = 0

			# Record
			if recording:
				rec_buffer.extend(data)

			# Stop recording after some time
			if recording and ( (time.time() > t_max) or (silence_count == 100) ):
				print("Stop Recording")
				recording = False
				#print(silence_count)
				break

		# Pack the recorded audio as wave
		data = struct.pack('<' + ('h'*len(rec_buffer)), *rec_buffer)
		sample_width = p.get_sample_size(pyaudio.paInt16)
		wf = wave.open("/home/pi/tmp/mic.wav", 'wb')
		wf.setnchannels(1)
		wf.setsampwidth(sample_width)
		wf.setframerate(44100)
		wf.writeframes(data)
		wf.close()

		# Open the just saved wave
		with sr.WavFile("/home/pi/tmp/mic.wav") as source:
			# Recognize the audio
			audio = recog.record(source)

		# Print the result if any
		try:
			result = recog.recognize(audio)
			print("You said: " + result)
			msg = OSC.OSCMessage()
			msg.setAddress("/recog/text")
			msg.append( result )
			client.send(msg)
		except LookupError:
			# speech is unintelligible
			print("Could not understand audio")

		stream.close()


if __name__ == "__main__":
	main()
