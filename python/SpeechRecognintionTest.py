import speech_recognition as sr

import pyaudio
import audioop
import time
from array import array
import struct
import wave

p = pyaudio.PyAudio()

for i in range(p.get_device_count()):
    print (p.get_device_info_by_index(i))

stream = p.open(format = pyaudio.paInt16,
    channels = 1,
    rate = 44100,
    input = True,
    frames_per_buffer = 256,
    input_device_index = 2)


recording = False
t_init = time.time() + 1
r = array('h')

while True:

    data = array('h',stream.read(256))
    rms = audioop.rms(data,2)

    if time.time() < t_init:
        continue

    if rms > 500 and not recording:
        print("Start Recording")
        recording = True
        t_end = time.time() + 8

    if recording:
        r.extend(data)

    if recording and (time.time() > t_end):
        print("Stop Recording")
        recording = False
        break

data = struct.pack('<' + ('h'*len(r)), *r)
sample_width = p.get_sample_size(pyaudio.paInt16)

wf = wave.open("/home/pi/vismumi/python/foo.wav", 'wb')
wf.setnchannels(1)
wf.setsampwidth(sample_width)
wf.setframerate(44100)
wf.writeframes(data)
wf.close()

#Then send the .WAV file to SR for transcription like so:

recog = sr.Recognizer("de-DE")
with sr.WavFile("foo.wav") as source:
    audio = recog.record(source)

try:
    print("You said: " + recog.recognize(audio))
except LookupError:
    # speech is unintelligible
    print("Could not understand audio")

